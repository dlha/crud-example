from flask import Blueprint, Flask, request
from flask_restx import Api
from flask_sqlalchemy import SQLAlchemy

# from blueprints.api import blueprint as api_blueprint
from config import Config


app = Flask(__name__)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config.from_object(Config)
# blueprint = Blueprint("api", __name__, url_prefix="/api")
# app.register_blueprint(blueprint)
db = SQLAlchemy(app, session_options={"autoflush": False})

api = Api(app, description="APIN Simple Item CRUD API", title="Item API", version="0.1")

from resources.Item import Item, ItemList, item_ns, items_ns


@app.before_first_request
def create_table():
    db.create_all()


api.add_namespace(item_ns)
api.add_namespace(items_ns)

items_ns.add_resource(Item, "/<int:id>")
item_ns.add_resource(ItemList, "/")

if __name__ == "__main__":
    app.run()
