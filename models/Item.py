from flask_sqlalchemy import SQLAlchemy
from main import db

class ItemModel(db.Model):
    __tablename__ = "items"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(320), nullable=False, unique=True)
    price = db.Column(db.Float(precision=2), nullable=False)

    def __init__(self, name, price) -> None:
        self.name = name
        self.price = price
    
    def __repr__(self) -> str:
        return f"ItemModel({self.price=}, {self.name=})"
