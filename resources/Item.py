from flask import jsonify, request
from flask_restx import Resource, fields, Namespace

from models.Item import ItemModel
from main import db

ITEM_NOT_FOUND = "Item not found!"

item_ns = Namespace("item", description="Item related operations")
items_ns = Namespace("item", description="Items related operations")

# RestPlus model
item = item_ns.model(
    "Item",
    {
        "id": fields.Integer(),
        "name": fields.String("Item's name"),
        "price": fields.Float(0.00),
    },
)

item_crud = item_ns.model(
    "Item",
    {
        "name": fields.String("Item's name"),
        "price": fields.Float(0.00),
    },
)


class Item(Resource):
    @item_ns.doc(description="Get Item with id")
    @item_ns.marshal_with(item)
    def get(self, id):
        item_data = ItemModel.query.filter(ItemModel.id == id).first()
        if item_data:
            return item_data
        else:
            return {"message": "Can't find item"}, 404

    @item_ns.expect(item_crud)
    @item_ns.marshal_with(item_crud)
    def put(self, id):
        data = request.get_json()
        item = ItemModel.query.filter(ItemModel.id == id).first()
        item.name = data["name"]
        item.price = data["price"]
        db.session.add(item)
        db.session.commit()
        return item

    @item_ns.response(204, "Item deleted")
    def delete(self, id):
        ItemModel.query.filter(ItemModel.id == id).delete()
        db.session.commit()
        return "", 204


class ItemList(Resource):
    """
    Show a list of all Items, POST to create new item
    """

    @items_ns.doc(description="Show a list of all Items")
    @items_ns.marshal_list_with(item)
    def get(self):
        item_list = ItemModel.query.all()
        return item_list

    @item_ns.doc(description="Create new Item")
    @item_ns.expect(item_crud)
    @item_ns.marshal_with(item_crud)
    def post(self):
        item_data = ItemModel(**request.get_json())
        print(type(request.json))
        db.session.add(item_data)
        db.session.commit()
        return item_data, 201
